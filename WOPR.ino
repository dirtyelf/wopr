#include <FastLED.h>

// Timer Values
// 256 prescaler: 1s 62500, .5s 31250
const uint16_t t4_compA = 62500;
const uint16_t t4_compB = 31250;

// 64 prescaler: .25s 62500
const uint16_t t5_comp = 62500;

// Number of LEDs in each group
#define NUM_LEDS_RIGHT 59
#define NUM_LEDS_SIDE 102
#define NUM_LEDS_FRONT 186
#define NUM_LEDS_BACK 186

// Which pin is each group connected to
#define DATA_PIN_RIGHT 4
#define DATA_PIN_SIDE 5
#define DATA_PIN_FRONT 6
#define DATA_PIN_BACK 7

// Define the arrays of LEDs
CRGB right_panel[NUM_LEDS_RIGHT];
CRGB side_panel[NUM_LEDS_SIDE];
CRGB front_panel[NUM_LEDS_FRONT];
CRGB back_panel[NUM_LEDS_BACK];

// Define the colors to be used
CRGB red = {244, 0, 0};
CRGB yellow = {255, 255, 0};
CRGB black = {0, 0, 0};

// Toggle LED Flags
bool runFlag = false;
uint8_t runNine = 0;
uint8_t runSix = 0;
volatile bool toggleHalfFlag = false;
volatile bool toggleFullFlag = false;
volatile bool changeFlag = false;

void setup() {
  noInterrupts();
  TCCR4A = 0;
  TCCR4B = 0;
  TCNT4 = 0;
  OCR4A = t4_compA;
  OCR4B = t4_compB;
  TCCR4B |= (1 << CS42);    // 256 prescaler for t4
  TIMSK4 |= (1 << OCIE4A);  // enable t4 compA interrupt
  TIMSK4 |= (1 << OCIE4B);  // enable t4 compB interrupt

  TCCR5A = 0;
  TCCR5B = 0;
  TCNT5 = 0;
  OCR5A = t5_comp;
  TCCR5A |= (1 << WGM52);   // CTC Mode for t5
  TCCR5B |= (1 << CS51);    // 64 prescaler for t5
  TCCR5B |= (1 << CS50);
  TIMSK5 |= (1 << OCIE5A);  // enable t5 compA interrupt
  interrupts();

  FastLED.addLeds<WS2812B, DATA_PIN_RIGHT, RGB>(right_panel, NUM_LEDS_RIGHT);
  FastLED.addLeds<WS2812B, DATA_PIN_SIDE, RGB>(side_panel, NUM_LEDS_SIDE);
  FastLED.addLeds<WS2812B, DATA_PIN_FRONT, RGB>(front_panel, NUM_LEDS_FRONT);
  FastLED.addLeds<WS2812B, DATA_PIN_BACK, RGB>(back_panel, NUM_LEDS_BACK);
  // set master brightness 0-255
  FastLED.setBrightness(18); // 7%

  // set static LEDs
  staticRightPanel();
  staticSidePanel();
  staticFrontPanel();
  staticBackPanel();

  // preset the offset toggle LEDs
  presetFrontPanel();
  presetBackPanel();
  presetSidePanel();
  presetRightPanel();

  FastLED.show();
}

void loop() {
  if (runFlag) {
    runners();
    runSix++;
    runNine++;
    if (runNine > 8) {
      runNine = 0;
    }
    if (runSix > 5) {
      runSix = 0;
    }
    runFlag = false;
    changeFlag = true;
  }

  if (toggleHalfFlag) {
    halfToggles();
    toggleHalfFlag = false;
    changeFlag = true;
  }

  if (toggleFullFlag) {
    fullToggles();
    toggleFullFlag = false;
    changeFlag = true;
  }

  if (changeFlag) {
    FastLED.show();
    changeFlag = false;
  }
}

ISR(TIMER4_COMPA_vect) {
  // 1 second interrupt
  TCNT4 = 0;
  // toggle the 1 and .5 sec leds
  toggleHalfFlag = true;
  toggleFullFlag = true;
}

ISR(TIMER4_COMPB_vect) {
  // .5 second interrupt
  // toggle the .5 sec leds
  toggleHalfFlag = true;
}

ISR(TIMER5_COMPA_vect) {
  // .25 second interrupt
  // progress the running led sections by one
  runFlag = true;
}

void fullToggles() {
  fullSecFrontPanel();
  fullSecBackPanel();
  fullSecSidePanel();
  fullSecRightPanel();
}

void halfToggles() {
  halfSecFrontPanel();
  halfSecBackPanel();
  halfSecSidePanel();
}

void runners() {
  frontPanelRunner();
  backPanelRunner();
  rightPanelRunners();
}
