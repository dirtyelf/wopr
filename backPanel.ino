void staticBackPanel () {
  int numRedLeds = 59;
  int redLeds[] = {
    9, 10, 13, 14, 15, 16, 29, 31, 39, 40,
    41, 42, 44, 45, 61, 62, 68, 69, 80, 83,
    84, 85, 91, 93, 100, 105, 108, 109, 110, 111,
    113, 123, 124, 127, 128, 130, 131, 132, 133, 136,
    139, 143, 146, 148, 150, 152, 154, 156, 157, 166,
    168, 172, 173, 174, 177, 182, 184, 185, 186
  };
  for (int i = 0; i < numRedLeds; i++) {
    back_panel[redLeds[i]-1] = red;
  }

  int numYellowLeds = 16;
  int yellowLeds[] = {
    19, 20, 57, 58, 59, 64, 65, 67, 87, 88,
    101, 102, 116, 117, 137, 159
  };
  for (int i = 0; i < numYellowLeds; i++) {
    back_panel[yellowLeds[i]-1] = yellow;
  }
}

// Preset the inverse toggle leds to on so that they will toggle inverse of the other set
void presetBackPanel() {
  int numRedLedsA = 8;
  int redLedsA[] = {25, 26, 97, 118, 129, 145, 160, 180};
  for (int i = 0; i < numRedLedsA; i++) {
    back_panel[redLedsA[i]-1] = red;
  }

  int numYellowLedsA = 7;
  int yellowLedsA[] = {4, 5, 32, 36, 55, 152, 182};
  for (int i = 0; i < numYellowLedsA; i++) {
    back_panel[yellowLedsA[i]-1] = yellow;
  }
}

void fullSecBackPanel() {
  int numRedLedsA = 8;
  int redLedsA[] = {25, 26, 97, 118, 129, 145, 160, 180};
  int numRedLedsB = 10;
  int redLedsB[] = {21, 22, 94, 95, 114, 115, 126, 149, 158, 176};
  for (int i = 0; i < numRedLedsA; i++) {
    if (back_panel[redLedsA[i]-1]) {
      back_panel[redLedsA[i]-1] = black;
    } else {
      back_panel[redLedsA[i]-1] = red;
    }
  }
  for (int i = 0; i < numRedLedsB; i++) {
    if (back_panel[redLedsB[i]-1]) {
      back_panel[redLedsB[i]-1] = black;
    } else {
      back_panel[redLedsB[i]-1] = red;
    }
  }

  int numYellowLedsA = 7;
  int yellowLedsA[] = {4, 5, 32, 36, 55, 152, 182};
  int numYellowLedsB = 7;
  int yellowLedsB[] = {1, 2, 33, 34, 56, 120, 121};
  for (int i = 0; i < numYellowLedsA; i++) {
    if (back_panel[yellowLedsA[i]-1]) {
      back_panel[yellowLedsA[i]-1] = black;
    } else {
      back_panel[yellowLedsA[i]-1] = yellow;
    }
  }
  for (int i = 0; i < numYellowLedsB; i++) {
    if (back_panel[yellowLedsB[i]-1]) {
      back_panel[yellowLedsB[i]-1] = black;
    } else {
      back_panel[yellowLedsB[i]-1] = yellow;
    }
  }
}

void halfSecBackPanel () {
  int numRedLeds = 3;
  int blinkRed[] = {107, 161, 170};
  for (int i = 0; i < numRedLeds; i++) {
    if (back_panel[blinkRed[i]-1]) {
      back_panel[blinkRed[i]-1] = black;
    } else {
      back_panel[blinkRed[i]-1] = red;
    }
  }

  int numYellowLeds = 2;
  int blinkYellow[] = {90, 165};
  for (int i = 0; i < numYellowLeds; i++) {
    if (back_panel[blinkYellow[i]-1]) {
      back_panel[blinkYellow[i]-1] = black;
    } else {
      back_panel[blinkYellow[i]-1] = yellow;
    }
  }
}

void backPanelRunner () {
  int numYellowLeds = 9;
  int runYellow[] = {78, 77, 76, 75, 74, 73, 72, 71, 70};
  for (int i = 0; i < numYellowLeds; i++) {
    if (i == runNine) {
      back_panel[runYellow[i]-1] = yellow;
    } else {
      back_panel[runYellow[i]-1] = black;
    }
  }
}
