void staticFrontPanel () {
  int numRedLeds = 59;
  int redLeds[] = {
    1, 3, 16, 17, 18, 19, 22, 23, 32, 33,
    49, 50, 52, 53, 54, 55, 63, 65, 71, 72,
    73, 75, 87, 88, 94, 95, 105, 107, 108, 109,
    110, 113, 118, 126, 129, 130, 132, 134, 137, 141,
    144, 147, 148, 149, 150, 152, 153, 156, 157, 158,
    159, 165, 168, 169, 170, 174, 176, 185, 186
  };
  for (int i = 0; i < numRedLeds; i++) {
    front_panel[redLeds[i]-1] = red;
  }

  int numYellowLeds = 16;
  int yellowLeds[] = {
    12, 13, 35, 36, 37, 68, 69, 89, 91, 92,
    101, 102, 116, 117, 143, 183
  };
  for (int i = 0; i < numYellowLeds; i++) {
    front_panel[yellowLeds[i]-1] = yellow;
  }
}

// Preset the inverse toggle leds to on so that they will toggle inverse of the other set
void presetFrontPanel() {
  int numRedLedsA = 8;
  int redLedsA[] = {6, 7, 100, 121, 135, 151, 162, 182};
  for (int i = 0; i < numRedLedsA; i++) {
    front_panel[redLedsA[i]-1] = red;
  }

  int numYellowLedsA = 7;
  int yellowLedsA[] = {27, 28, 39, 58, 62, 128, 160};
  for (int i = 0; i < numYellowLedsA; i++) {
    front_panel[yellowLedsA[i]-1] = yellow;
  }
}

void fullSecFrontPanel() {
  int numRedLedsA = 8;
  int redLedsA[] = {6, 7, 100, 121, 135, 151, 162, 182};
  int numRedLedsB = 10;
  int redLedsB[] = {10, 11, 103, 104, 123, 124, 131, 154, 166, 184};
  for (int i = 0; i < numRedLedsA; i++) {
    if (front_panel[redLedsA[i]-1]) {
      front_panel[redLedsA[i]-1] = black;
    } else {
      front_panel[redLedsA[i]-1] = red;
    }
  }
  for (int i = 0; i < numRedLedsB; i++) {
    if (front_panel[redLedsB[i]-1]) {
      front_panel[redLedsB[i]-1] = black;
    } else {
      front_panel[redLedsB[i]-1] = red;
    }
  }

  int numYellowLedsA = 7;
  int yellowLedsA[] = {27, 28, 39, 58, 62, 128, 160};
  int numYellowLedsB = 7;
  int yellowLedsB[] = {30, 31, 38, 60, 61, 97, 98};
  for (int i = 0; i < numYellowLedsA; i++) {
    if (front_panel[yellowLedsA[i]-1]) {
      front_panel[yellowLedsA[i]-1] = black;
    } else {
      front_panel[yellowLedsA[i]-1] = yellow;
    }
  }
  for (int i = 0; i < numYellowLedsB; i++) {
    if (front_panel[yellowLedsB[i]-1]) {
      front_panel[yellowLedsB[i]-1] = black;
    } else {
      front_panel[yellowLedsB[i]-1] = yellow;
    }
  }
}

void halfSecFrontPanel () {
  int numRedLeds = 3;
  int blinkRed[] = {115, 178, 181};
  for (int i = 0; i < numRedLeds; i++) {
    if (front_panel[blinkRed[i]-1]) {
      front_panel[blinkRed[i]-1] = black;
    } else {
      front_panel[blinkRed[i]-1] = red;
    }
  }

  int numYellowLeds = 2;
  int blinkYellow[] = {66, 173};
  for (int i = 0; i < numYellowLeds; i++) {
    if (front_panel[blinkYellow[i]-1]) {
      front_panel[blinkYellow[i]-1] = black;
    } else {
      front_panel[blinkYellow[i]-1] = yellow;
    }
  }
}

void frontPanelRunner () {
  int numYellowLeds = 9;
  int runYellow[] = {78, 79, 80, 81, 82, 83, 84, 85, 86};
  for (int i = 0; i < numYellowLeds; i++) {
    if (i == runNine) {
      front_panel[runYellow[i]-1] = yellow;
    } else {
      front_panel[runYellow[i]-1] = black;
    }
  }
}
