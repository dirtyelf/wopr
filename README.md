# WOPR
This is the arduino code for a custom computer build modeled after the WOPR computer from the movie War Games.

The build log can be found here: https://www.overclock.net/forum/18082-builds-logs-case-mods/1643969-scratch-wopr-casemod-randomdesign.html

## Latest Code Updates

** aug 8 **

* Initial Commits

** aug 11 **

* Static LEDs should light at 15% brightness.

* All blinking LEDs should work

* Running LEDs should work

** aug 27 **

* subtract 1 from the LED numbers because the LED arrays are zero based

* set brightness to 7%

* set red to 244,0,0 and yellow to 255,255,0

* timer setup changes for timer 5
