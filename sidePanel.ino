void staticSidePanel () {
  int numRedLeds = 39;
  int redLeds[] = {
    2, 3, 7, 8, 9, 12, 16, 18, 21, 24,
    26, 27, 33, 34, 37, 38, 43, 48, 50, 53,
    54, 58, 59, 64, 65, 66, 67, 68, 70, 71,
    75, 78, 81, 82, 86, 89, 94, 95, 100
  };
  for (int i = 0; i < numRedLeds; i++) {
    side_panel[redLeds[i]-1] = red;
  }

  int numYellowLeds = 10;
  int yellowLeds[] = {
    10, 19, 32, 45, 46, 52, 60, 72, 85, 102
  };
  for (int i = 0; i < numYellowLeds; i++) {
    side_panel[yellowLeds[i]-1] = yellow;
  }
}

// Preset the inverse toggle leds to on so that they will toggle inverse of the other set
void presetSidePanel() {
  int numRedLedsA = 8;
  int redLedsA[] = {5, 6, 35, 57, 73, 87, 88, 91};
  for (int i = 0; i < numRedLedsA; i++) {
    side_panel[redLedsA[i]-1] = red;
  }
}

// toggle leds on and off every second
void fullSecSidePanel () {
  int numRedLedsA = 8;
  int redLedsA[] = {5, 6, 35, 57, 73, 87, 88, 91};
  for (int i = 0; i < numRedLedsA; i++) {
    if (side_panel[redLedsA[i]-1]) {
      side_panel[redLedsA[i]-1] = black;
    } else {
      side_panel[redLedsA[i]-1] = red;
    }
  }

  int numRedLedsB = 7;
  int redLedsB[] = {22, 23, 36, 57, 69, 74, 90};
  for (int i = 0; i < numRedLedsB; i++) {
    if (side_panel[redLedsB[i]-1]) {
      side_panel[redLedsB[i]-1] = black;
    } else {
      side_panel[redLedsB[i]-1] = red;
    }
  }
}

void halfSecSidePanel () {
  int numRedLeds = 5;
  int blinkRed[] = {11, 39, 40, 51, 97};
  for (int i = 0; i < numRedLeds; i++) {
    if (side_panel[blinkRed[i]-1]) {
      side_panel[blinkRed[i]-1] = black;
    } else {
      side_panel[blinkRed[i]-1] = red;
    }
  }

  int numYellowLeds = 2;
  int blinkYellow[] = {55, 83};
  for (int i = 0; i < numYellowLeds; i++) {
    if (side_panel[blinkYellow[i]-1]) {
      side_panel[blinkYellow[i]-1] = black;
    } else {
      side_panel[blinkYellow[i]-1] = yellow;
    }
  }
}
