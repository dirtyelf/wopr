// Set the static LEDs on once
void staticRightPanel () {
  int numRedLeds = 13;
  int redLeds[] = {
    17, 20, 24, 26, 27, 30, 32, 34, 37, 38,
    41, 44, 45
  };
  for (int i = 0; i < numRedLeds; i++) {
    right_panel[redLeds[i]-1] = red;
  }

  int numYellowLeds = 3;
  int yellowLeds[] = {22, 35, 42};
  for (int i = 0; i < numYellowLeds; i++) {
    right_panel[yellowLeds[i]-1] = yellow;
  }
}

// Preset the inverse toggle leds to on so that they will toggle inverse of the other set
void presetRightPanel() {
  int numRedLedsA = 5;
  int redLedsA[] = {1, 5, 7, 8, 9};
  for (int i = 0; i < numRedLedsA; i++) {
    right_panel[redLedsA[i]-1] = red;
  }
}

// toggle leds on and off every second
void fullSecRightPanel () {
  int numRedLedsA = 5;
  int redLedsA[] = {1, 5, 7, 8, 9};
  for (int i = 0; i < numRedLedsA; i++) {
    if (right_panel[redLedsA[i]-1]) {
      right_panel[redLedsA[i]-1] = black;
    } else {
      right_panel[redLedsA[i]-1] = red;
    }
  }

  int numRedLedsB = 7;
  int redLedsB[] = {2, 4, 6, 8, 10, 11, 15};
  for (int i = 0; i < numRedLedsB; i++) {
    if (right_panel[redLedsB[i]-1]) {
      right_panel[redLedsB[i]-1] = black;
    } else {
      right_panel[redLedsB[i]-1] = red;
    }
  }
}

// progress the running leds
void rightPanelRunners () {
  int numYellowLedsA = 6;
  int runYellowA[] = {48, 49, 50, 51, 52, 53};
  for (int i = 0; i < numYellowLedsA; i++) {
    if (i == runSix) {
      right_panel[runYellowA[i]-1] = yellow;
    } else {
      right_panel[runYellowA[i]-1] = black;
    }
  }

  int numYellowLedsB = 6;
  int runYellowB[] = {59, 58, 57, 56, 55, 54};
  for (int i = 0; i < numYellowLedsB; i++) {
    if (i == runSix) {
      right_panel[runYellowB[i]-1] = yellow;
    } else {
      right_panel[runYellowB[i]-1] = black;
    }
  }
}
